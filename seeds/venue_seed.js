const faker = require('faker');

function createFakeVenue() {
  return {
    name: faker.name.findName(),
    description: faker.lorem.text(),
  }
}

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('venue').del()
    .then(function () {
      const venues = [];

      for(let i = 0; i < 100; i++) {
        venues.push(createFakeVenue());
      }
      // Inserts seed entries
      return knex('venue').insert(venues);
    });
};
