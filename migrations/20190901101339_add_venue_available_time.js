
exports.up = function(knex) {
  return knex.schema.table('venue', function(table) {
    table.integer('day1_start');
    table.integer('day1_end');
    table.integer('day2_start');
    table.integer('day2_end');
    table.integer('day3_start');
    table.integer('day3_end');
    table.integer('day4_start');
    table.integer('day4_end');
    table.integer('day5_start');
    table.integer('day5_end');
    table.integer('day6_start');
    table.integer('day6_end');
    table.integer('day7_start');
    table.integer('day7_end');
  });
};

exports.down = function(knex) {
  
};
