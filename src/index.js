const { ApolloServer, gql } = require('apollo-server');
const setting = require('./../knexfile.js')
const venueResolvers = require('./VenueResolvers');
const inventoryResolvers = require('./InventoryResolvers');
const knex = require('knex')(setting.development);

// The GraphQL Schema
const typeDefs = gql`
  type Query {
    venue(id: Int): Venue
    venueList: [Venue]
    inventory(id: Int) : Inventory
    inventoryList: [Inventory]
  }

  type Mutation {
    createVenue(data: VenueInput): Int
    updateVenue(id: Int, data: VenueInput): Boolean
    createInventory(data: InventoryInput): Int
    updateInventory(id: Int, data: InventoryInput): Boolean
  }

  type Venue {
    id: Int
    name: String
    description: String
  }

  type Inventory {
    id: Int
    venue_id: Int
    name: String
    description: String
  }

  input VenueInput {
    name: String
    description: String
  }

  input InventoryInput{
    venue_id: Int
    name: String
    description: String
  }
`;

// A map of functions which return data for the schema.
const resolvers = [ venueResolvers, inventoryResolvers];

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: () => {
    return {
      knex: knex
    };
  }
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`)
});