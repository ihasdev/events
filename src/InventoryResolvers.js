async function InventoryListResolver(_, __, ctx) {
  const result = await ctx.knex.select([ 'id','venue_id', 'name', 'description' ]).from('Inventory');
  return result;
}

async function CreateInventoryResolver(_, { data }, ctx) {
  const result = await ctx.knex('inventory').insert(data);
  return result[0];
}

async function InventoryResolver(_, { id }, ctx) {
  const result = await ctx.knex('inventory').where({ id }).select([ 'id','venue_id', 'name', 'description' ]);
  if (result.length > 0) return result[0];
  return null;
}

async function UpdateInventoryResolver(_, { id, data }, ctx) {
  await ctx.knex('inventory').where({ id }).update(data);
  return true;
}

module.exports = {
  Query: {
    inventory: InventoryResolver,
    inventoryList: InventoryListResolver
  },

  Mutation: {
    updateInventory: UpdateInventoryResolver,
    createInventory: CreateInventoryResolver
  }
};