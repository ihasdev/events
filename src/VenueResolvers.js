async function VenueListResolver(_, __, ctx) {
  const result = await ctx.knex.select([ 'id', 'name', 'description' ]).from('venue');
  return result;
}

async function CreateVenueResolver(_, { data }, ctx) {
  const result = await ctx.knex('venue').insert(data);
  return result[0];
}

async function VenueResolver(_, { id }, ctx) {
  const result = await ctx.knex('venue').where({ id }).select([ 'id', 'name', 'description' ]);
  if (result.length > 0) return result[0];
  return null;
}

async function UpdateVenueResolver(_, { id, data }, ctx) {
  await ctx.knex('venue').where({ id }).update(data);
  return true;
}

module.exports = {
  Query: {
    venue: VenueResolver,
    venueList: VenueListResolver
  },

  Mutation: {
    updateVenue: UpdateVenueResolver,
    createVenue: CreateVenueResolver
  }
};